#include <iostream>
#include <cmath>
#include <string>
using namespace std;

int main()
{
	char isbn[14];
	int isbn_number[10];

	cout << "Please enter the isbn code: ";
	cin >> isbn;
	cin.get();

	int i = 0;
	int j = 0;

	while (isbn[i] != '\0')
	{
		if (isbn[i] >= '0' && isbn[i] <= '9')
		{
			isbn_number[j++] = isbn[i] - '0';
		}
		i++;
	}

	int sum = 0;
	for (i = 0, j = 10; i < 10; i++, j--)
	{
		cout << "(" << j << " * " << isbn_number[i] << ") + ";
		sum += j * isbn_number[i];
	}
	cout << "\nThe sum is " << sum << endl;
	cout << "The ISBN number is " << ((sum % 11) ? " not " : " ") << " correct " << endl;

	cin.get();
}